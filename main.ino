#define btnPin 3

enum led {
  RED = 2,
  YELLOW = 4,
  GREEN = 7
};

enum state {
  EMERGENCY,
  STOP,
  GET_READY,
  GO,
  BRAKE
};

enum rgb {
  czer = 11,
  ziel = 10,
  nieb = 9
};

int tm; //czas absolutny od włączenia układu
int last_tm; //czas ostatniej zmiany stanu układu, potrzebny do spraedzania czy upłynął już czas danego stanu świateł

struct state_leds{
  state s; //nazwa stanu
  int dl; //czas w s trwania stanu
  led combination[3]; //kombinacja diód w danym stanie
};

struct state_rgb{
  int ds;
  rgb kombinacja[3];
  };

state_rgb staty[] = {{2,czer},{2,czer,ziel},{2,ziel},{2,ziel,nieb},{2,nieb},{2,czer,nieb}};
rgb rgbs[] = {czer, ziel, nieb};
state_rgb *actual_rgb = &staty[0];

state_leds states[] = {{EMERGENCY, 5}, {STOP, 12, RED}, {GET_READY, 2, RED, YELLOW}, {GO, 14, GREEN}, {BRAKE, 2, YELLOW}};
led leds[] = {RED, YELLOW, GREEN};
state_leds *actual_state = &states[STOP];

void setup() {
  for(int i = 0;i < 3;i++) pinMode(leds[i], OUTPUT);
  for(int i = 0;i <3;i++) pinMode(rgbs[i], OUTPUT);
  pinMode(btnPin, INPUT_PULLUP);
  tm = 0;
  last_tm = tm;
  update_from_state(actual_state);
  updateRGB(actual_rgb);
}

void loop() {
  delay(1000);
  tm++;
  if(!(tm%2)) actual_rgb++;
  if(actual_rgb - staty > sizeof(staty)/sizeof(state_rgb) /* długość tablicy stanów rgb */ - 1) actual_rgb = staty;
  if(tm - last_tm == actual_state->dl){
    last_tm = tm;
    actual_state++;
  }
  if((!digitalRead(btnPin))&&actual_state==&states[GO]){
    for(int i = 0;i < 3;i++) digitalWrite(rgbs[i], HIGH);
    int tmp = (actual_state->dl) - (tm - last_tm); //czas pozostały do zmiany światła na żółte wg normalnego cyklu
    int maks = states[EMERGENCY].dl - states[BRAKE].dl; //badamy ile czasu możemy maksymalnie zwlekać ze zmianą światła na żółte żeby zdążyć w zadanym czasie
    int _time_to_wait = tmp > maks ? maks : tmp; //wybieramy mniejszą z wartości: czas maksymalny maks oraz czas do normalnej zmiany świateł tmp
    actual_state = &states[EMERGENCY];
    delay(_time_to_wait*1000);
    tm+=_time_to_wait;
    update_from_state(&states[BRAKE]); //obsługujemy stan BRAKE bez fizycznego włączania go
    delay(1000*(states[BRAKE].dl));
    tm+=(states[BRAKE].dl);
    last_tm = tm;
    actual_state++; //przechodzimy do stanu STOP (który jest następny w tablicy stanów po EMERGENCY)
  }
  updateRGB(actual_rgb);
  if(actual_state - states > sizeof(states)/sizeof(state_leds) /* długość tablicy stanów led */ - 1) actual_state = &states[STOP];
  update_from_state(actual_state);
}


void update_from_state(state_leds *st){
  for(int i = 0;i < 3;i++) digitalWrite(leds[i], LOW);
  for(int i = 0;i < 3;i++) if(st->combination[i]) digitalWrite(st->combination[i], HIGH);
}

void updateRGB(state_rgb *st){
  for(int i = 0;i < 3;i++) digitalWrite(rgbs[i], LOW);
  for (int i = 0;i < 3;i++) if(st->kombinacja[i]) digitalWrite(st->kombinacja[i], HIGH);
}
